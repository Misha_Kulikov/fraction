public class Fraction {
    public int denominator;
    public int numerator;
    private int wholePart;


    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction() {
        this(1, 1);
    }


    public static void FractionToString(Fraction f1) {
        System.out.printf("Результат выражения равен %d/%d %n", f1.numerator, f1.denominator);
    }
}
