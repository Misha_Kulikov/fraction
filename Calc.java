import java.util.Scanner;

public class Calc {
    private static int n;
    private static int d;
    public static Fraction add(Fraction f1, Fraction f2){
        Fraction f3;
        d = f1.denominator;
        if (f1.denominator == f2.denominator) {
            n= f1.numerator + f2.numerator;
        } else if (f1.denominator != f2.denominator) {
            n = (f1.numerator * f2.denominator) + (f2.numerator * f1.denominator);
            d = f1.denominator * f2.denominator;
        }
        return f3 = new Fraction(n,d);
    }
    public static Fraction h2(Fraction f1, Fraction f2) {
        Fraction f3;
        d = f1.denominator;
        if (f1.denominator == f2.denominator) {
            n = f1.numerator - f2.numerator;
        } else if (f1.denominator != f2.denominator) {
            n = (f1.numerator * f2.denominator) - (f2.numerator * f1.denominator);
            d = f1.denominator * f2.denominator;
        }
        return f3 = new Fraction(n,d);
    }
    public static Fraction h4(Fraction f1, Fraction f2){
        Fraction f3;
        n = f1.numerator * f2.denominator;
        d =f1.denominator * f2.numerator;
        return f3 = new Fraction(n,d);

    }
    public static Fraction h3(Fraction f1, Fraction f2){
        Fraction f3;
        n = f1.numerator * f2.numerator;
        d = f1.denominator * f2.denominator;
        return f3 = new Fraction(n,d);
    }
    public static Fraction reductionOfFraction(Fraction f1){
        int min = Math.abs(f1.numerator);
        int max = f1.denominator;
        for(int i = min; i > 1; i--){
            if((min%i==0) &&(max%i==0)){
                n=f1.numerator/i;
                d=f1.denominator/i;
            }

        }
        return f1 = new Fraction(n,d);
    }
}